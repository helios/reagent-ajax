# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased][unreleased]

## 0.1.2 - 2017-02-05
### Changed
 - Updated dependencies
 - Added `output-dir` to `prod` profile, due to an error of figwheel.

## 0.1.1 - 2016-01-24
### Changed
 - Updated dependencies

## 0.1.0 - 2015-12-13
### Added
 - Project created

[Unreleased]:
https://github.com/Heliosmaster/reagent-ajax/compare/v0.1.0...HEAD
